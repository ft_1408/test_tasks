<?php
include "src/class/Price.php";
include "src/class/Words.php";


$products = [
		"A" => 10, 
		"B" => 20, 
		"C" => 30, 
		"D" => 40, 
		"E" => 50, 
		"F" => 60, 
		"G" => 70, 
		"H" => 80, 
		"I" => 90, 
		"J" => 100, 
		"K" => 110, 
		"L" => 120, 
		"M" => 130
	];

// Can be transformet to yml file for more simplification
$discountRules = [
	// Discount 1
	[
		'goods' => ['A', 'B'],
		'discount' => 10
	],
	// Discount 2
	[
		'goods' => ['D', 'E'],
		'discount' => 5
	],

	// Discount 3
	[
		'goods' => ['E', 'F', 'G'],
		'discount' => 5
	],

	//Discount 4
	[
		'static_good' => "A",
		'goods' => ['K', "L", "M"],
		'discount' => 5
	],
	[
		'exclusions' => ['A', 'C'],
		'countDiscounts' => [
			['goods_count' => 3, 'discount'=> 5],
			['goods_count' => 4, 'discount'=> 10],
			['goods_count' => 5, 'discount'=> 20],
		]
	]

];

// Discount
$price = new Price();

$price->setPriceList($products);
$price->setDiscountRules($discountRules);
$price->setGoodList(["A", "L","E", 'F', 'G']);

// echo "\n";
// print_r($price->getPrice());


// Words
$phrase = "{Пожалуйста|Просто} сделайте так, чтобы это {удивительное|крутое|простое} тестовое предложение изменялось {быстро|мгновенно} {случайным образом|менялось каждый раз}";

$words = new Words();
$words->setInitialPhrase($phrase);

print_r($words->getPhrase());

