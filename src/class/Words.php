<?php
class Words{
	private $phrase;

	public function setInitialPhrase($phrase){
		$this->phrase = $phrase;
	}

	public function getPhrase(){

		preg_match_all("/{([^}]*)}/", $this->phrase, $matches);
		$phrase = $this->phrase;

		foreach ($matches[1] as $mach) {
			$machArray = explode('|', $mach);
			$num = rand (0, count($machArray) -1);
			
			$machReplace = '{' . $mach . '}';
		
			$phrase  = str_replace($machReplace, $machArray[$num], $phrase);
		}

		return $phrase;
	}
}