<?php

class Price {
	private $priceList;
	private $discountRules;
	private $goodsList;
	private $usedGoods;

	public function setPriceList(array $priceList){
		$this->priceList = $priceList;
	}

	public function setDiscountRules(array $discountRules){
		$this->discountRules = $discountRules;
	}

	public function setGoodList(array $goodsList){
		$this->goodsList = $goodsList;
	}


	public function getPrice(){
		$discount = $this->getDiscount();
		$sum = $this->getGoodsSum();

		return $sum - $discount;
	}

	private function getGoodsSum(){
		$totalSum = 0;

		foreach ($this->goodsList as $good) {
			$totalSum += $this->priceList[$good];
		}

		return $totalSum;
	}

	private function getGoodCountForPromotions(array $exclusions){
		$counter = 0;
		foreach ($this->goodsList as $good) {
			if(!in_array($good, $exclusions)){
				$counter++;
			}
		}

		return $counter;
	}

	private function getDiscount(){
		$totalDiscount = 0;
		$countValues = array_count_values($this->goodsList);
		$this->usedGoods = [];

		foreach ($this->discountRules as $rule) {
			$goodsSumm = 0;
			$minRepeat = max(array_values($countValues));

			// If we have static good
			if(array_key_exists('static_good', $rule)){
				// If we have that good in selected goods check one of
				// goods that shuld be with it
				if(array_key_exists($rule['static_good'] , $countValues)){
					foreach ($rule['goods'] as $good) {
						// We need only one good from that
						if(array_key_exists ($good , $countValues)){
							$this->usedGoods[] = $good;
							$goodsSumm = $this->priceList[$good];
							$minRepeat = 1;
							break;
						}
					}
				}
			}elseif(array_key_exists('countDiscounts', $rule)){
				$discountPersent = 0;

				// If we have some exclusions
				if(array_key_exists('exclusions', $rule)){
					$selectedGoodsCount = $this->getGoodCountForPromotions($rule['exclusions']);
				}else{
					$selectedGoodsCount = count($this->goodsList);
				}

				foreach ($rule['countDiscounts'] as $countDiscountRule) {
					if($selectedGoodsCount >= $countDiscountRule['goods_count']){
						$discountPersent = $countDiscountRule['discount'];
					}	
				}

				$rule['discount'] = $discountPersent;
				$goodsSumm = $this->getGoodsSum(); 
			}	
			else{
				foreach ($rule['goods'] as $good) {
					// Counting summ of goods from promotion
					$goodsSumm += $this->priceList[$good];

					if(array_key_exists ($good , $countValues) && !in_array($good, $this->usedGoods)){
						$this->usedGoods[] = $good;
						// Minimum repiting from goods list in promotions
						// will be number of reppiting 1 promotion 
						if($countValues[$good] < $minRepeat){
							$minRepeat = $countValues[$good];
						}	
					}else{
						// If one of goods not exists in list min repet = 0 (not repinting in list)
						$minRepeat = 0;
					}
				}	
			}

			// Getting discount of 1 pair
			$discount = $goodsSumm * $rule['discount'] / 100;
			// Getting discount of n pairs and add it to total discount
			$totalDiscount += $discount * $minRepeat;  
		}

		return $totalDiscount;
	}

} 